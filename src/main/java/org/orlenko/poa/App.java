package org.orlenko.poa;

import org.orlenko.poa.configuration.AppConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication(scanBasePackages = {"org.orlenko.poa.service", "org.orlenko.poa.controller", "org.orlenko.poa.factory"})
@Import(AppConfig.class)
public class App {

	public static void main(String[] args) {
		run(App.class, args);
	}
}
