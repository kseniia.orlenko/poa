package org.orlenko.poa.model.card;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CreditCard extends Card {
	@JsonProperty("monhtlyLimit")
	private Integer monthlyLimit;
}
