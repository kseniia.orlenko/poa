package org.orlenko.poa.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.orlenko.poa.model.card.CardReference;
import org.orlenko.poa.model.type.Authorization;
import org.orlenko.poa.model.type.Direction;

import java.util.List;

@Data
@Accessors(chain = true)
public class PowerOfAttorney {
	private String id;
	private String grantor;
	private String grantee;
	private String account;
	private Direction direction;
	private List<Authorization> authorizations;
	private List<CardReference> cards;
}
