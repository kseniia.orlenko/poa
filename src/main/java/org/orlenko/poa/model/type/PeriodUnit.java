package org.orlenko.poa.model.type;

public enum PeriodUnit {
	PER_DAY, PER_WEEK, PER_MONTH;
}
