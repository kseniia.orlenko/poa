package org.orlenko.poa.configuration;

import org.orlenko.poa.configuration.properties.PropertiesConfig;
import org.orlenko.poa.configuration.swagger.SwaggerConfig;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import static java.time.Duration.ofMillis;

@Configuration
@Import({SwaggerConfig.class, PropertiesConfig.class})
public class AppConfig {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder
			.setConnectTimeout(ofMillis(3000))
			.setReadTimeout((ofMillis(3000)))
			.build();
	}
}
