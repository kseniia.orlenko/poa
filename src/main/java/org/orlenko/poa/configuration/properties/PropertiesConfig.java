package org.orlenko.poa.configuration.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfig {

	@Value("${powerOfAttorney.store.baseUrl: http://localhost:8080}")
	private String storeBaseUrl;

	@Bean
	public AppProperties appProperties() {
		return AppProperties.builder()
			.storeBaseUrl(storeBaseUrl)
			.build();
	}
}
