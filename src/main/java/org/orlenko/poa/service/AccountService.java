package org.orlenko.poa.service;

import org.orlenko.poa.model.account.Account;

import java.util.Optional;

public interface AccountService {

	Optional<Account> getAccount(String id);
}
