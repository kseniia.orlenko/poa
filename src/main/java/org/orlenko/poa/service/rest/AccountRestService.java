package org.orlenko.poa.service.rest;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.orlenko.poa.configuration.properties.AppProperties;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.service.AccountService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountRestService implements AccountService {

	private static final String ACCOUNTS = "%s/accounts/%s";

	@NonNull
	private final RestTemplate restTemplate;
	@NonNull
	private final AppProperties appProperties;

	@Override
	public Optional<Account> getAccount(String id) {
		log.trace("Get account with id '{}'", id);
		Account account = restTemplate.getForObject(format(ACCOUNTS, appProperties.getStoreBaseUrl(), id.substring(8)), Account.class);
		if (account != null && (account.getEnded() == null || LocalDate.now().isBefore(account.getEnded()))) {
			return of(account);
		}
		log.warn("Account with '{}' is invalid!", id);
		return empty();
	}
}
