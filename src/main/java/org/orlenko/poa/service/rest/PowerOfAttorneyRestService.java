package org.orlenko.poa.service.rest;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.orlenko.poa.configuration.properties.AppProperties;
import org.orlenko.poa.exception.ResourceNotFoundException;
import org.orlenko.poa.factory.CardServiceFactory;
import org.orlenko.poa.model.PowerOfAttorney;
import org.orlenko.poa.model.PowerOfAttorneyReference;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.service.AccountService;
import org.orlenko.poa.service.CardService;
import org.orlenko.poa.service.PowerOfAttorneyService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpMethod.GET;

@Slf4j
@Service
@RequiredArgsConstructor
public class PowerOfAttorneyRestService implements PowerOfAttorneyService {

	private static final String POWER_OF_ATTORNEYS = "/power-of-attorneys";
	private static final String SEPARATOR = "/";

	@NonNull
	private final RestTemplate restTemplate;
	@NonNull
	private final AppProperties appProperties;
	@NonNull
	private final AccountService accountService;
	@NonNull
	private final CardServiceFactory cardServiceFactory;

	public List<PowerOfAttorneyView> getPowerOfAttorneys() {
		ArrayList<PowerOfAttorneyView> result = new ArrayList<>();
		ofNullable(getPowerOfAttorneyReferences()).orElseThrow(() -> new ResourceNotFoundException("Can't find Power of Attorney references"))
			.forEach(powerOfAttorneyReference -> {
				PowerOfAttorney powerOfAttorney = ofNullable(getPowerOfAttorney(powerOfAttorneyReference))
					.orElseThrow(() -> new ResourceNotFoundException(format("Can't find Power of Attorney for id '%s'", powerOfAttorneyReference.getId())));
				PowerOfAttorneyView powerOfAttorneyView = new PowerOfAttorneyView().setGrantor(powerOfAttorney.getGrantor())
					.setGrantee(powerOfAttorney.getGrantee());
				accountService.getAccount(powerOfAttorney.getAccount()).ifPresent(account -> {
					powerOfAttorneyView.setAccount(account);
					ofNullable(powerOfAttorney.getCards()).ifPresent(cards -> cards.forEach(cardReference -> powerOfAttorney.getAuthorizations()
						.stream()
						// check that user can view the card
						.filter(authorization -> authorization.name().equals(cardReference.getType().name()))
						.findAny()
						.ifPresent(authorization -> {
							CardService cardService = cardServiceFactory.getCardServiceByType(cardReference.getType());
							cardService.getCard(cardReference.getId()).ifPresent(card -> powerOfAttorneyView.getCards().add(card));
						})));
				});
				result.add(powerOfAttorneyView);
			});
		return result;
	}

	@Nullable
	private PowerOfAttorney getPowerOfAttorney(PowerOfAttorneyReference powerOfAttorneyReference) {
		return restTemplate.getForObject(getPowerOfAttorneyByIdUrl(powerOfAttorneyReference.getId()), PowerOfAttorney.class);
	}

	@Nullable
	private List<PowerOfAttorneyReference> getPowerOfAttorneyReferences() {
		return restTemplate.exchange(
			appProperties.getStoreBaseUrl() + POWER_OF_ATTORNEYS,
			GET,
			null,
			new ParameterizedTypeReference<List<PowerOfAttorneyReference>>() {}
		).getBody();
	}

	private String getPowerOfAttorneyByIdUrl(String id) {
		return appProperties.getStoreBaseUrl() + POWER_OF_ATTORNEYS + SEPARATOR + id;
	}
}
