package org.orlenko.poa.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.service.PowerOfAttorneyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PowerOfAttorneyController {

	@NonNull
	private final PowerOfAttorneyService powerOfAttorneyService;

	@GetMapping("power-of-attorneys")
	public List<PowerOfAttorneyView> getPowerOfAttorneys() {
		return powerOfAttorneyService.getPowerOfAttorneys();
	}
}
