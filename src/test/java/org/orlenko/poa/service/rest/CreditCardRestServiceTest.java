package org.orlenko.poa.service.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CreditCard;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.orlenko.poa.configuration.properties.AppProperties.builder;
import static org.orlenko.poa.model.type.Status.ACTIVE;
import static org.orlenko.poa.model.type.Status.BLOCKED;
import static org.orlenko.poa.service.rest.TestUtils.CARD_ID_2;
import static org.orlenko.poa.service.rest.TestUtils.getCreditCard;

@RunWith(MockitoJUnitRunner.class)
public class CreditCardRestServiceTest {

	private static final String HTTP_LOCAL_HOST = "http://localhost:8080";
	private static final String CREDIT_CARDS = "%s/credit-cards/%s";

	@Mock
	private RestTemplate restTemplate;

	private CreditCardRestService creditCardRestService;

	@Before
	public void setUp() {
		creditCardRestService = new CreditCardRestService(restTemplate, builder().storeBaseUrl(HTTP_LOCAL_HOST).build());
	}

	@Test
	public void getCard() {
		Card expectedCard = getCreditCard(ACTIVE);
		when(restTemplate.getForObject(getUrl(), CreditCard.class)).thenReturn((CreditCard) expectedCard);

		Optional<Card> card = creditCardRestService.getCard(CARD_ID_2);
		assertThat(card).isNotEmpty();
		assertThat(card).hasValue(expectedCard);
	}

	@Test
	public void getCardIsNotActive() {
		Card expectedCard = getCreditCard(BLOCKED);

		when(restTemplate.getForObject(getUrl(), CreditCard.class)).thenReturn((CreditCard) expectedCard);

		Optional<Card> card = creditCardRestService.getCard(CARD_ID_2);
		assertThat(card).isEmpty();
	}

	private String getUrl() {
		return format(CREDIT_CARDS, HTTP_LOCAL_HOST, CARD_ID_2);
	}
}