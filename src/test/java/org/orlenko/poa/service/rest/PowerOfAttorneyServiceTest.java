package org.orlenko.poa.service.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.orlenko.poa.configuration.properties.AppProperties;
import org.orlenko.poa.exception.ResourceNotFoundException;
import org.orlenko.poa.factory.CardServiceFactory;
import org.orlenko.poa.model.PowerOfAttorney;
import org.orlenko.poa.model.PowerOfAttorneyReference;
import org.orlenko.poa.model.PowerOfAttorneyView;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CardReference;
import org.orlenko.poa.model.type.Authorization;
import org.orlenko.poa.model.type.CardType;
import org.orlenko.poa.model.type.Status;
import org.orlenko.poa.service.AccountService;
import org.orlenko.poa.service.PowerOfAttorneyService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.orlenko.poa.model.type.Authorization.CREDIT_CARD;
import static org.orlenko.poa.model.type.Authorization.DEBIT_CARD;
import static org.orlenko.poa.model.type.Authorization.VIEW;
import static org.orlenko.poa.model.type.Direction.GIVEN;
import static org.orlenko.poa.service.rest.TestUtils.CARD_ID_1;
import static org.orlenko.poa.service.rest.TestUtils.CARD_ID_2;
import static org.orlenko.poa.service.rest.TestUtils.getAccount;
import static org.orlenko.poa.service.rest.TestUtils.getCreditCard;
import static org.orlenko.poa.service.rest.TestUtils.getDebitCard;
import static org.springframework.http.HttpMethod.GET;

@RunWith(MockitoJUnitRunner.class)
public class PowerOfAttorneyServiceTest {

	private static final String HTTP_LOCAL_HOST = "http://local§host:8080";
	private static final String POWER_OF_ATTORNEYS_REFERENCE = "/power-of-attorneys";
	private static final String POWER_OF_ATTORNEYS = "/power-of-attorneys/%s";
	private static final String POA_ID = "01";
	private static final String ACCOUNT_ID = "001111";
	private static final String GRANTOR = "Grantor";
	private static final String GRANTEE = "Grantee";

	@Mock
	private RestTemplate restTemplate;
	@Mock
	private AccountService accountService;
	@Mock
	private CardServiceFactory cardServiceFactory;
	@Mock
	private DebitCardRestService debitCardRestService;
	@Mock
	private CreditCardRestService creditCardRestService;

	private PowerOfAttorneyService powerOfAttorneyService;

	@Before
	public void setUp() {
		AppProperties properties = AppProperties.builder().storeBaseUrl(HTTP_LOCAL_HOST).build();
		powerOfAttorneyService = new PowerOfAttorneyRestService(restTemplate, properties, accountService, cardServiceFactory);
		when(cardServiceFactory.getCardServiceByType(CardType.CREDIT_CARD)).thenReturn(creditCardRestService);
		when(cardServiceFactory.getCardServiceByType(CardType.DEBIT_CARD)).thenReturn(debitCardRestService);
	}

	@Test
	public void getPowerOfAttorneysWithActiveAccountAndDebitCardCreditCardView() {
		List<Authorization> authorizations = asList(DEBIT_CARD, CREDIT_CARD, VIEW);
		List<CardReference> cardReferences = asList(
			new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD),
			new CardReference().setId(CARD_ID_2).setType(CardType.CREDIT_CARD)
		);
		PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
		Card expectedDebitCard = getDebitCard(Status.ACTIVE);
		Account expectedAccount = getAccount(null);
		Card expectedCreditCard = getCreditCard(Status.ACTIVE);
		mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
		mockPOA(expectedPOA);
		mockDebitCard(expectedDebitCard);
		mockCreditCard(expectedCreditCard);
		mockAccount(expectedAccount);

		List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

		assertEquals(1, powerOfAttorneys.size());
		PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
		assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
		assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
		assertEquals(expectedAccount, actualPOA.getAccount());
		assertEquals(2, actualPOA.getCards().size());
		assertTrue(actualPOA.getCards().contains(expectedCreditCard));
		assertTrue(actualPOA.getCards().contains(expectedDebitCard));
	}

	@Test
	public void getPowerOfAttorneysWithActiveAccountAndDebitCardView() {
		List<Authorization> authorizations = asList(DEBIT_CARD, VIEW);
		List<CardReference> cardReferences = asList(
			new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD),
			new CardReference().setId(CARD_ID_2).setType(CardType.CREDIT_CARD)
		);
		PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
		Card expectedCard = getDebitCard(Status.ACTIVE);
		Account expectedAccount = getAccount(null);
		mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
		mockPOA(expectedPOA);
		mockDebitCard(expectedCard);
		mockAccount(expectedAccount);

		List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

		assertEquals(1, powerOfAttorneys.size());
		PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
		assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
		assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
		assertEquals(expectedAccount, actualPOA.getAccount());
		assertEquals(1, actualPOA.getCards().size());
		assertEquals(expectedCard, actualPOA.getCards().get(0));
		verifyZeroInteractions(creditCardRestService);
	}

	@Test
	public void getPowerOfAttorneysWithInactiveAccount() {
		List<Authorization> authorizations = asList(DEBIT_CARD, VIEW);
		List<CardReference> cardReferences = singletonList(new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD));
		PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
		mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
		mockPOA(expectedPOA);
		mockAccount(null);

		List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

		assertEquals(1, powerOfAttorneys.size());
		PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
		assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
		assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
		assertNull(actualPOA.getAccount());
		verifyZeroInteractions(creditCardRestService);
		verifyZeroInteractions(debitCardRestService);
	}

	@Test
	public void getPowerOfAttorneysWithActiveAccountAndInactiveDebitCard() {
		List<Authorization> authorizations = asList(DEBIT_CARD, VIEW);
		List<CardReference> cardReferences = asList(
			new CardReference().setId(CARD_ID_1).setType(CardType.DEBIT_CARD),
			new CardReference().setId(CARD_ID_2).setType(CardType.CREDIT_CARD)
		);
		PowerOfAttorney expectedPOA = getPowerOfAttorney(authorizations, cardReferences);
		Account expectedAccount = getAccount(null);
		mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
		mockPOA(expectedPOA);
		mockAccount(expectedAccount);
		mockDebitCard(null);

		List<PowerOfAttorneyView> powerOfAttorneys = powerOfAttorneyService.getPowerOfAttorneys();

		assertEquals(1, powerOfAttorneys.size());
		PowerOfAttorneyView actualPOA = powerOfAttorneys.get(0);
		assertEquals(expectedPOA.getGrantor(), actualPOA.getGrantor());
		assertEquals(expectedPOA.getGrantee(), actualPOA.getGrantee());
		assertEquals(expectedAccount, actualPOA.getAccount());
		assertEquals(0, actualPOA.getCards().size());
		verify(debitCardRestService).getCard(CARD_ID_1);
		verifyZeroInteractions(creditCardRestService);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void getPowerOfAttorneysNullFromReferenceEndpoint() {
		mockPowerOfAttorneys(null);

		powerOfAttorneyService.getPowerOfAttorneys();
	}

	@Test(expected = ResourceNotFoundException.class)
	public void getPowerOfAttorneysNullFromPoverOfAttorneyEndpoint() {
		mockPowerOfAttorneys(singletonList(new PowerOfAttorneyReference().setId(POA_ID)));
		mockPOA(null);

		powerOfAttorneyService.getPowerOfAttorneys();
	}

	private void mockAccount(Account expectedAccount) {
		when(accountService.getAccount(ACCOUNT_ID)).thenReturn(Optional.ofNullable(expectedAccount));
	}

	private void mockDebitCard(Card expectedCard) {
		when(debitCardRestService.getCard(CARD_ID_1)).thenReturn(Optional.ofNullable(expectedCard));
	}

	private void mockCreditCard(Card expectedCard) {
		when(creditCardRestService.getCard(CARD_ID_2)).thenReturn(Optional.ofNullable(expectedCard));
	}

	private void mockPOA(PowerOfAttorney expectedPOA) {
		when(restTemplate.getForObject(format(HTTP_LOCAL_HOST + POWER_OF_ATTORNEYS, POA_ID), PowerOfAttorney.class)).thenReturn(expectedPOA);
	}

	private PowerOfAttorney getPowerOfAttorney(List<Authorization> authorizations, List<CardReference> cardReferences) {
		return new PowerOfAttorney().setId(POA_ID).setGrantor(GRANTOR).setGrantee(GRANTEE).setAccount(ACCOUNT_ID)
			.setDirection(GIVEN).setAuthorizations(authorizations).setCards(cardReferences);
	}

	private void mockPowerOfAttorneys(List result) {
		when(restTemplate.exchange(
			HTTP_LOCAL_HOST + POWER_OF_ATTORNEYS_REFERENCE,
			GET,
			null,
			new ParameterizedTypeReference<List<PowerOfAttorneyReference>>() {
			}
		)).thenReturn(new ResponseEntity<>(result, HttpStatus.OK));
	}
}