package org.orlenko.poa.service.rest;

import lombok.experimental.UtilityClass;
import org.orlenko.poa.model.account.Account;
import org.orlenko.poa.model.card.Card;
import org.orlenko.poa.model.card.CreditCard;
import org.orlenko.poa.model.card.DebitCard;
import org.orlenko.poa.model.card.Limit;
import org.orlenko.poa.model.type.PeriodUnit;
import org.orlenko.poa.model.type.Status;

import java.time.LocalDate;

import static java.time.LocalDate.now;

@UtilityClass
public class TestUtils {

	public static final String CARD_ID_1 = "001";
	public static final String CARD_ID_2 = "002";

	private static final String OWNER = "Owner";
	private static final Long BALANCE = 700L;
	private static final LocalDate CREATED = now().minusYears(1);
	private static final int CARD_NUMBER = 12347;
	private static final String HOLDER = "Holder";

	public static Account getAccount(LocalDate ended) {
		return new Account().setOwner(OWNER).setBalance(BALANCE).setCreated(CREATED).setEnded(ended);
	}

	public static Card getDebitCard(Status status) {
		return new DebitCard().setContactLess(true)
			.setPosLimit(new Limit().setLimit(24).setPeriodUnit(PeriodUnit.PER_WEEK))
			.setAtmLimit(new Limit().setLimit(100).setPeriodUnit(PeriodUnit.PER_DAY))
			.setId(CARD_ID_1)
			.setCardNumber(CARD_NUMBER)
			.setCardHolder(HOLDER)
			.setStatus(status)
			.setSequenceNumber(1);
	}

	public static Card getCreditCard(Status status) {
		return new CreditCard().setId(CARD_ID_2).setCardNumber(CARD_NUMBER).setCardHolder(HOLDER)
			.setStatus(status).setSequenceNumber(1);
	}
}
